


// Task-1 Tabbing

$(document).ready(function () {

    $('li').click(function () {
        if ($('.tab_main').hasClass('show')) {
            $('.tabcontent').eq($(this).index()).removeClass('show');   
            $('.tablinks').eq($(this).index()).removeClass('active');    
          
        } else{
            $('.tabcontent').eq($(this).index()).addClass('show').parent().siblings().find('.tabcontent').removeClass('show'); // Display's the element associated on clicking the button, and removes others.

            $('.tablinks').eq($(this).index()).addClass('active').parent().siblings().find('.tablinks').removeClass('active'); // Changes color of the clicked button

        }
    })    
})

//Task-2 Accordion

$(document).ready(function () {
    $('.title').click(function () {
 
     if ($('.title .sub').eq($(this).index()).hasClass('active')) {
         $('.title .sub').removeClass('active');
         $('.title .sign').removeClass('show');
     }
     
     else{
 
        $('.title .sub').eq($(this).index()).addClass('active').parents().siblings().find('.sub').removeClass('active');     // Displays the panel of selected button, and hides the remaining ones.
        
        $('.title .sign').eq($(this).index()).addClass('show').parents().siblings().find('.sign').removeClass('show');       // Changes sign of the panel when, it opens
    
     }
       
 
    })
 
 
 
 })